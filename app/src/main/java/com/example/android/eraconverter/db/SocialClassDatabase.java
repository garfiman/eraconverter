package com.example.android.eraconverter.db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.android.eraconverter.dao.SocialClassDao;
import com.example.android.eraconverter.entity.SocialClass;

@Database(entities = {SocialClass.class}, version = 2, exportSchema = false)
public abstract class SocialClassDatabase extends RoomDatabase {


    private static SocialClassDatabase SC_INSTANCE;
    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {
                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new SocialClassDatabase.PopulateDbAsync(SC_INSTANCE).execute();
                }
            };

    public static SocialClassDatabase getDatabase(final Context context) {
        if (SC_INSTANCE == null) {
            synchronized (SocialClassDatabase.class) {
                if (SC_INSTANCE == null) {
                    SC_INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            SocialClassDatabase.class, "social_class_database")
                            .fallbackToDestructiveMigration()
                            .allowMainThreadQueries()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return SC_INSTANCE;
    }

    public abstract SocialClassDao socialClassDao();

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final SocialClassDao mDao;

        PopulateDbAsync(SocialClassDatabase db) {
            mDao = db.socialClassDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {

            if (mDao.getAnySocialClass().length < 1) {

                mDao.insert(new SocialClass("fourth", "Magyarország 2020", 650000.0, "forint", "orvos", "nagy ház, nyaraló, drága autó, külföldi nyaralások"));
                mDao.insert(new SocialClass("third", "Magyarország 2020", 350000.0, "forint", "könyvelő", "saját családi ház, új autó, külföldi nyaralás"));
                mDao.insert(new SocialClass("second", "Magyarország 2020", 170000.0, "forint", "tanár", "saját lakás, használt autó, évi egyszeri nyaralás"));
                mDao.insert(new SocialClass("first", "Magyarország 2020", 90000.0, "forint", "közmunkás", "bérelt lakás, régi (örökölt) autó, nem jár nyaralni"));
                mDao.insert(new SocialClass("fourth", "Német-Római Császárság XV. század", 100.0, "arany", "földesúr", "földbirtok, jobbágyoktól terményjáradék, kereskedelmi bevételek"));
                mDao.insert(new SocialClass("third", "Német-Római Császárság XV. század", 30.0, "arany", "kereskedő", "városi ház, lerakat, kereskedő hajók"));
                mDao.insert(new SocialClass("second", "Német-Római Császárság XV. század", 3.0, "arany", "katona", "fegyverzet, páncélzat, szálláshely, élelem"));
                mDao.insert(new SocialClass("first", "Német-Római Császárság XV. század", 0.0, "arany", "jobbágy", "jobbágytelek, házhely, de ez évente változhat, saját megának termelt élelmiszer és tenyésztett állatok"));
                mDao.insert(new SocialClass("fourth", "Ókori Róma Kr.e. 4. század", 83340.0, "sestertius", "szenátor", "villa, fürdők látogatása, bor fogyasztás, lakomák, rabszolgák"));
                mDao.insert(new SocialClass("third", "Ókori Róma Kr.e. 4. század", 5000.0, "sestertius", "főtisztviselő - sexagenarius", "fegyverzet, páncélzat, élelem, szállás"));
                mDao.insert(new SocialClass("second", "Ókori Róma Kr.e. 4. század", 200.0, "sestertius", "pék", "béren felüli ellátás, ház, rabszolga"));
                mDao.insert(new SocialClass("first", "Ókori Róma Kr.e. 4. század", 0.0, "sestertius", "rabszolga", "élelem, ágy, ruha, amiket a tartójától kap"));

            }
            return null;
        }
    }
}
