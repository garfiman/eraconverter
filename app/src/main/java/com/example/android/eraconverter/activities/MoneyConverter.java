package com.example.android.eraconverter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.android.eraconverter.R;
import com.example.android.eraconverter.viewmodel.ExchangeRateViewModel;

import java.util.ArrayList;
import java.util.List;

public class MoneyConverter extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private EditText mInitialCurrencyBox;
    private TextView mResultCurrencyBox;

    private String fromCurr;
    private String toCurr;

    private List<String> spinnerArray = new ArrayList<>();

    private double exchangeRateDouble = 0;

    private ExchangeRateViewModel mExchangeRateViewModel;
    private ArrayAdapter<String> spinnerToArrayAdapter;
    private Spinner spinnerFrom;
    private Spinner spinnerTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_converter);

        mInitialCurrencyBox = findViewById(R.id.convert_initial);
        mResultCurrencyBox = findViewById(R.id.convert_result);

        mExchangeRateViewModel = ViewModelProviders.of(this).get(ExchangeRateViewModel.class);

        spinnerFrom = findViewById(R.id.spinnerCurrencyFrom);
        if (spinnerFrom != null) {
            spinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    //set fromStr on selected item
                    fromCurr = spinnerFrom.getSelectedItem().toString();
                    //if the other is non null set that one too
                    if (spinnerTo.getSelectedItem() != null) {
                        toCurr = spinnerTo.getSelectedItem().toString();

                        if (mExchangeRateViewModel.getExchangeRateBetweenTwoCurrenciesSimple(fromCurr, toCurr) != null) {
                            exchangeRateDouble = mExchangeRateViewModel.getExchangeRateBetweenTwoCurrenciesSimple(fromCurr, toCurr);
                        } else {
                            exchangeRateDouble = Double.NEGATIVE_INFINITY;
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    //do nothing
                }
            });
        }

        spinnerTo = findViewById(R.id.spinnerCurrencyTo);
        if (spinnerTo != null) {
            spinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    toCurr = spinnerTo.getSelectedItem().toString();
                    //if the other is nonnull set that one too
                    if (spinnerTo.getSelectedItem() != null) {
                        fromCurr = spinnerFrom.getSelectedItem().toString();

                        if (mExchangeRateViewModel.getExchangeRateBetweenTwoCurrenciesSimple(fromCurr, toCurr) != null) {
                            exchangeRateDouble = mExchangeRateViewModel.getExchangeRateBetweenTwoCurrenciesSimple(fromCurr, toCurr);
                        } else {
                            exchangeRateDouble = Double.NEGATIVE_INFINITY;
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    //do nothing
                }

            });
        }

        final Observer<List<String>> spinnerObserver = new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable final List<String> newSpinnerArray) {
                spinnerToArrayAdapter.clear();
                spinnerArray = newSpinnerArray;
                for (String currVal : newSpinnerArray) {
                    spinnerToArrayAdapter.add(currVal);
                }
            }
        };

        mExchangeRateViewModel.getAllCurrencies().observe(this, spinnerObserver);

        spinnerToArrayAdapter = new ArrayAdapter<>
                (this, android.R.layout.simple_spinner_item, spinnerArray);

        spinnerTo.setAdapter(spinnerToArrayAdapter);
        spinnerFrom.setAdapter(spinnerToArrayAdapter);
    }


    public void convertCurrency(View view) {
        String mCurr = mInitialCurrencyBox.getText().toString();
        if(TextUtils.isEmpty(mCurr)) {
            mInitialCurrencyBox.setError("Nem írtál be összeget");
            return;
        }

        if (exchangeRateDouble == Double.NEGATIVE_INFINITY) {
            Toast toast = Toast.makeText(this.getApplicationContext(), "Érvénytelen konverzió, válassz más pénznemeket", Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        double initialCurrencyAmount = getValue(mInitialCurrencyBox);

        mResultCurrencyBox.setText(String.valueOf(initialCurrencyAmount * exchangeRateDouble));
    }

    @NonNull
    private static Double getValue(EditText editText) {
        return Double.valueOf(editText.getText().toString());
    }

    public void openExchangeRateDb(View view) {
        Intent intent = new Intent(this, ExchangeRateDbViewActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
