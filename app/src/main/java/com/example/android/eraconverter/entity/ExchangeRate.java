package com.example.android.eraconverter.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;

@Entity(tableName = "exchange_rate_table", primaryKeys = {"currencyOne", "currencyTwo"})
public class ExchangeRate {

    @NonNull
    private String currencyOne;

    @NonNull
    private String currencyTwo;

    private Double rate;

    @NonNull
    public String getCurrencyOne(){return this.currencyOne;}
    @NonNull
    public String getCurrencyTwo(){return this.currencyTwo;}
    public Double getRate(){return this.rate;}


    public ExchangeRate(@NonNull String currencyOne, @NonNull String currencyTwo, double rate) {
        this.currencyOne = currencyOne;
        this.currencyTwo = currencyTwo;
        this.rate = rate;
    }

    @NonNull
    @Override
    public String toString() {
        return "1 " + currencyOne + " = " + rate.toString() + " " + currencyTwo;
    }
}
