package com.example.android.eraconverter.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.android.eraconverter.db.ExchangeRateRepository;
import com.example.android.eraconverter.entity.ExchangeRate;

import java.util.List;

public class ExchangeRateViewModel extends AndroidViewModel {

    private ExchangeRateRepository mRepository;
    private LiveData<List<ExchangeRate>> mAllExchangeRates;
    private LiveData<List<String>> mAllCurrencies;

    public ExchangeRateViewModel(@NonNull Application application) {
        super(application);
        mRepository = new ExchangeRateRepository(application);
        mAllExchangeRates = mRepository.getAllExchangeRates();
        mAllCurrencies = mRepository.getAllCurrencies();
    }

    public LiveData<List<ExchangeRate>> getAllExchangeRates() {
        return mAllExchangeRates;
    }

    public LiveData<List<String>> getAllCurrencies() { return  mAllCurrencies;}

    public Double getExchangeRateBetweenTwoCurrenciesSimple(String currOne, String currTwo) {
        return mRepository.getExchangeRateBetweenTwoCurrenciesSimple(currOne, currTwo);
    }

    public void insert(ExchangeRate exchangeRate) { mRepository.insert(exchangeRate); }

    public void deleteExchangeRate(ExchangeRate exchangeRate) {mRepository.deleteExchangeRate(exchangeRate);}

}
