package com.example.android.eraconverter.activities;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android.eraconverter.R;

public class EraConvertResult extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_era_convert_result);

        TextView mNowEra = findViewById(R.id.now_era);
        TextView mThenEra = findViewById(R.id.then_era);

        TextView mNowIncome = findViewById(R.id.now_income);
        TextView mThenIncome = findViewById(R.id.then_income);

        TextView mNowJob = findViewById(R.id.now_job);
        TextView mThenJob = findViewById(R.id.then_job);

        TextView mNowDesc = findViewById(R.id.now_desc);
        TextView mThenDesc = findViewById(R.id.then_desc);

        TextView mNowCurr = findViewById(R.id.now_curr);
        TextView mThenCurr = findViewById(R.id.then_curr);

        Bundle bundle = getIntent().getExtras().getBundle("erasBundle");

        mNowEra.setText(bundle.getString("nowEra"));
        mThenEra.setText(bundle.getString("thenEra"));

        mNowIncome.setText(String.valueOf(bundle.getDouble("nowIncome")));
        mThenIncome.setText(String.valueOf(bundle.getDouble("thenIncome")));

        mNowCurr.setText(bundle.getString("nowCurrency"));
        mThenCurr.setText(bundle.getString("thenCurrency"));

        mNowJob.setText(bundle.getString("nowJob"));
        mThenJob.setText(bundle.getString("thenJob"));

        mNowDesc.setText(bundle.getString("nowProperty"));
        mThenDesc.setText(bundle.getString("thenProperty"));

    }
}
