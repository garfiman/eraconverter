package com.example.android.eraconverter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.eraconverter.R;
import com.example.android.eraconverter.entity.SocialClass;

import java.util.List;

public class SocialClassListAdapter extends RecyclerView.Adapter<SocialClassListAdapter.SocialClassViewHolder> {

    private final LayoutInflater mInflater;
    private List<SocialClass> mSocialClasses;

    public SocialClassListAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }


    @NonNull
    @Override
    public SocialClassViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.socialclass_recyclerview_item, parent, false);
        return new SocialClassViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SocialClassViewHolder holder, int position) {
        if (mSocialClasses != null) {
            SocialClass current = mSocialClasses.get(position);

            holder.ageAndLocationView.setText(current.getLocationAndAge());
            holder.quartileView.setText(String.format("%s quartile", current.getQuartile()));
            holder.jobView.setText(current.getJob());
            holder.currencyAmountView.setText(String.valueOf(current.getAvgIncome()));
            holder.currencyTypeView.setText(String.format("%s/hónap", current.getCurrencyType()));
            holder.descriptionView.setText(current.getPropertyDesc());

        } else {
            holder.ageAndLocationView.setText(R.string.no_age_location);
            holder.quartileView.setText(R.string.no_quartile);
            holder.jobView.setText(R.string.no_job);
            holder.currencyAmountView.setText(R.string.no_currency_amount);
            holder.currencyTypeView.setText(R.string.no_currency_type);
            holder.descriptionView.setText(R.string.no_description);
        }
    }

    public void setSocialClasses(List<SocialClass> sc) {
        mSocialClasses = sc;
        notifyDataSetChanged();
    }

    public SocialClass getSocialClassAtPosition(int position) {
        return mSocialClasses.get(position);
    }

    @Override
    public int getItemCount() {
        if (mSocialClasses != null)
            return mSocialClasses.size();
        else return 0;
    }

    class SocialClassViewHolder extends RecyclerView.ViewHolder {
        private final TextView ageAndLocationView;
        private final TextView quartileView;
        private final TextView jobView;
        private final TextView currencyAmountView;
        private final TextView currencyTypeView;
        private final TextView descriptionView;

        private SocialClassViewHolder(View itemView) {
            super(itemView);

            ageAndLocationView = itemView.findViewById(R.id.socialclass_name_ri);
            quartileView = itemView.findViewById(R.id.socialclass_quartile_ri);
            jobView = itemView.findViewById(R.id.socialclass_job_ri);
            currencyAmountView = itemView.findViewById(R.id.socialclass_currencyam_ri);
            currencyTypeView = itemView.findViewById(R.id.socialclass_currency_ri);
            descriptionView = itemView.findViewById(R.id.socialclass_desc_ri);
        }
    }


}
