package com.example.android.eraconverter.db;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.android.eraconverter.dao.ExchangeRateDao;
import com.example.android.eraconverter.entity.ExchangeRate;

import java.util.List;

public class ExchangeRateRepository {

    private ExchangeRateDao mExchangeRateDao;
    private LiveData<List<ExchangeRate>> mAllExchangeRates;
    private LiveData<List<String>> mAllCurrencies;

    public ExchangeRateRepository(Application application) {
        ExchangeRateDatabase db = ExchangeRateDatabase.getDatabase(application);
        mExchangeRateDao = db.ExchangeRateDao();
        mAllExchangeRates = mExchangeRateDao.getAllExchangeRates();
        mAllCurrencies = mExchangeRateDao.getAllCurrencies();
    }

    public LiveData<List<String>> getAllCurrencies() {
        return mAllCurrencies;
    }

    public LiveData<List<ExchangeRate>> getAllExchangeRates() {
        return mAllExchangeRates;
    }

    public void insert (ExchangeRate exchangeRate) {
        new insertAsyncTask(mExchangeRateDao).execute(exchangeRate);
    }

    public LiveData<Double> getExchangeRateBetweenTwoCurrencies(String currOne, String currTwo){
        return mExchangeRateDao.getExchangeRateBetweenTwoCurrencies(currOne, currTwo);
    }


    public Double getExchangeRateBetweenTwoCurrenciesSimple(String currOne, String currTwo) {
        return mExchangeRateDao.getExchangeRateBetweenTwoCurrenciesSimple(currOne, currTwo);
    }



    public void deleteExchangeRate(ExchangeRate exchangeRate)  {
        new deleteExchangeRateAsyncTask(mExchangeRateDao).execute(exchangeRate);
    }

    private static class insertAsyncTask extends AsyncTask<ExchangeRate, Void, Void> {

        private ExchangeRateDao mAsyncTaskDao;

        insertAsyncTask(ExchangeRateDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final ExchangeRate... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class deleteExchangeRateAsyncTask extends AsyncTask<ExchangeRate, Void, Void> {
        private ExchangeRateDao mAsyncTaskDao;

        deleteExchangeRateAsyncTask(ExchangeRateDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final ExchangeRate... params) {
            mAsyncTaskDao.deleteExchangeRate(params[0]);
            return null;
        }
    }



}
