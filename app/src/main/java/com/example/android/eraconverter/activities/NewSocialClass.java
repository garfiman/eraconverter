package com.example.android.eraconverter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android.eraconverter.R;

public class NewSocialClass extends AppCompatActivity {

    public static final String EXTRA_REPLY_BUNDLE =
            "com.example.android.eraconverter.BUNDLE";

    private EditText mLocAndAgeText;
    private EditText mIncomeValText;
    private EditText mIncomeCurrText;
    private EditText mJobNameText;
    private EditText mDescriptionText;
    private EditText mQuartileText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_social_class);

        mQuartileText = findViewById(R.id.new_socialclass_quartile);
        mLocAndAgeText = findViewById(R.id.new_socialclass_locationandage);
        mIncomeValText = findViewById(R.id.new_socialclass_income);
        mIncomeCurrText = findViewById(R.id.new_socialclass_curr);
        mJobNameText = findViewById(R.id.new_socialclass_jobname);
        mDescriptionText = findViewById(R.id.new_socialclass_desc);

        final Button button = findViewById(R.id.sc_button_save);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mQuartileText.getText()) ||
                        TextUtils.isEmpty(mLocAndAgeText.getText()) ||
                        TextUtils.isEmpty(mIncomeValText.getText()) ||
                        TextUtils.isEmpty(mIncomeCurrText.getText()) ||
                        TextUtils.isEmpty(mJobNameText.getText()) ||
                        TextUtils.isEmpty(mDescriptionText.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {


                    String mQuartile = mQuartileText.getText().toString();
                    String mLocAge = mLocAndAgeText.getText().toString();
                    String mIncCurr = mIncomeCurrText.getText().toString();

                    double mIncVal = Double.parseDouble(mIncomeValText.getText().toString());

                    String mJob = mJobNameText.getText().toString();
                    String mDesc = mDescriptionText.getText().toString();

                    Bundle bundle = new Bundle();


                    bundle.putString("quartile", mQuartile);
                    bundle.putString("locAge", mLocAge);
                    bundle.putString("incCurr", mIncCurr);
                    bundle.putString("job", mJob);
                    bundle.putString("desc", mDesc);

                    bundle.putDouble("incVal", mIncVal);

                    replyIntent.putExtra(EXTRA_REPLY_BUNDLE, bundle);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });

    }
}
