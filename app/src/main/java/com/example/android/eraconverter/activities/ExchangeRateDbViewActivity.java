package com.example.android.eraconverter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.eraconverter.R;
import com.example.android.eraconverter.adapter.ExchangeRateListAdapter;
import com.example.android.eraconverter.entity.ExchangeRate;
import com.example.android.eraconverter.viewmodel.ExchangeRateViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class ExchangeRateDbViewActivity extends AppCompatActivity {

    private ExchangeRateViewModel mExchangeRateViewModel;

    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;

    public static final String EXTRA_REPLY_BUNDLE =
            "com.example.android.eraconverter.BUNDLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange_rate_db_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final ExchangeRateListAdapter adapter = new ExchangeRateListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mExchangeRateViewModel = ViewModelProviders.of(this).get(ExchangeRateViewModel.class);
        mExchangeRateViewModel.getAllExchangeRates().observe(this, new Observer<List<ExchangeRate>>() {
            @Override
            public void onChanged(@Nullable final List<ExchangeRate> exchangeRate) {
                // Update the cached copy of the data in the adapter.
                adapter.setExchangeRates(exchangeRate);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ExchangeRateDbViewActivity.this, NewExchangeRate.class);
                startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST_CODE);
            }
        });
        

        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder,
                                         int direction) {
                        int position = viewHolder.getAdapterPosition();
                        ExchangeRate myExchangeRate = adapter.getExchangeRateAtPosition(position);
                        Toast.makeText(ExchangeRateDbViewActivity.this, "Adat törlése", Toast.LENGTH_SHORT).show();

                        // Delete the data
                        mExchangeRateViewModel.deleteExchangeRate(myExchangeRate);
                    }
                });
        helper.attachToRecyclerView(recyclerView);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Bundle bundle = data.getBundleExtra(EXTRA_REPLY_BUNDLE);
            assert bundle != null;
            String curr1 = bundle.getString("curr1");
            String curr2 = bundle.getString("curr2");
            double rate = bundle.getDouble("rate");

            assert curr1 != null;
            assert curr2 != null;
            ExchangeRate exchangeRate = new ExchangeRate(curr1, curr2, rate);
            mExchangeRateViewModel.insert(exchangeRate);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

}
