package com.example.android.eraconverter.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.android.eraconverter.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startMoneyConverter(View view) {
        Intent intent = new Intent(this, MoneyConverter.class);
        startActivity(intent);
    }

    public void startAbout(View view) {
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }

    public void startEraConverter(View view) {
        Intent intent = new Intent(this, EraConverter.class);
        startActivity(intent);
    }
}
