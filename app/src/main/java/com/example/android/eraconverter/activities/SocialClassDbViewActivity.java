package com.example.android.eraconverter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.eraconverter.R;
import com.example.android.eraconverter.adapter.SocialClassListAdapter;
import com.example.android.eraconverter.entity.SocialClass;
import com.example.android.eraconverter.viewmodel.SocialClassViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class SocialClassDbViewActivity extends AppCompatActivity {

    public static final int NEW_SOCIALCLASS_ACTIVITY_REQUEST_CODE = 2;

    public static final String EXTRA_REPLY_BUNDLE =
            "com.example.android.eraconverter.BUNDLE";

    private SocialClassViewModel mSocialClassViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_class_db_view);
        Toolbar toolbar = findViewById(R.id.socialclass_toolbar);
        setSupportActionBar(toolbar);

        RecyclerView recyclerView = findViewById(R.id.socialclass_recyclerview);
        final SocialClassListAdapter adapter = new SocialClassListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mSocialClassViewModel = ViewModelProviders.of(this).get(SocialClassViewModel.class);


        mSocialClassViewModel.getAllSocialClasses().observe(this, new Observer<List<SocialClass>>() {
            @Override
            public void onChanged(@Nullable final List<SocialClass> socialClasses) {
                adapter.setSocialClasses(socialClasses);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SocialClassDbViewActivity.this, NewSocialClass.class);
                startActivityForResult(intent, NEW_SOCIALCLASS_ACTIVITY_REQUEST_CODE);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        ItemTouchHelper helper = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(0,
                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                    @Override
                    public boolean onMove(@NonNull RecyclerView recyclerView,
                                          @NonNull RecyclerView.ViewHolder viewHolder,
                                          @NonNull RecyclerView.ViewHolder target) {
                        return false;
                    }

                    @Override
                    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder,
                                         int direction) {
                        int position = viewHolder.getAdapterPosition();
                        SocialClass mySocialClass = adapter.getSocialClassAtPosition(position);
                        Toast.makeText(SocialClassDbViewActivity.this, "Adat törlése", Toast.LENGTH_SHORT).show();

                        mSocialClassViewModel.deleteExchangeRate(mySocialClass);
                    }
                });
        helper.attachToRecyclerView(recyclerView);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_SOCIALCLASS_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Bundle bundle = data.getBundleExtra(EXTRA_REPLY_BUNDLE);
            assert bundle != null;
            String quartile = bundle.getString("quartile");
            String locAge = bundle.getString("locAge");
            String incCurr = bundle.getString("incCurr");
            String job = bundle.getString("job");
            String desc = bundle.getString("desc");

            double incVal = bundle.getDouble("incVal");

            assert quartile != null;
            assert locAge != null;
            assert incCurr != null;
            assert job != null;
            assert desc != null;

            SocialClass socialClass = new SocialClass(quartile, locAge, incVal, incCurr, job, desc);
            mSocialClassViewModel.insert(socialClass);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

}
