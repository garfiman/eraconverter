package com.example.android.eraconverter.db;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.android.eraconverter.dao.SocialClassDao;
import com.example.android.eraconverter.entity.SocialClass;

import java.util.List;

public class SocialClassRepository {

    private SocialClassDao mSocialClassDao;
    private LiveData<List<SocialClass>> mAllSocialClasses;
    private LiveData<List<String>> mAllEras;


    public SocialClassRepository(Application application) {
        SocialClassDatabase db = SocialClassDatabase.getDatabase(application);
        mSocialClassDao = db.socialClassDao();
        mAllSocialClasses = mSocialClassDao.getAllSocialClasses();
        mAllEras = mSocialClassDao.getAllEras();
    }

    public LiveData<List<String>> getAllEras() {
        return mAllEras;
    }

    public String getCurrentQuartile(double income, String era) {
        return mSocialClassDao.getCurrentQuartile(income, era);
    }

    public SocialClass getSCfromQuartile(String quartile, String era) {
        return mSocialClassDao.getSCfromQuartile(quartile, era);
    }

    public LiveData<List<SocialClass>> getAllSocialClasses() {
        return mAllSocialClasses;
    }

    public void insert(SocialClass socialClass) {
        new insertAsyncTask(mSocialClassDao).execute(socialClass);
    }

    public void deleteSocialClass(SocialClass socialClass) {
        new deleteSocialClassAsyncTask(mSocialClassDao).execute(socialClass);
    }

    private static class insertAsyncTask extends AsyncTask<SocialClass, Void, Void> {

        private SocialClassDao mAsyncTaskDao;

        insertAsyncTask(SocialClassDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(SocialClass... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class deleteSocialClassAsyncTask extends AsyncTask<SocialClass, Void, Void> {
        private SocialClassDao mAsyncTaskDao;

        deleteSocialClassAsyncTask(SocialClassDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final SocialClass... params) {
            mAsyncTaskDao.deleteSocialClass(params[0]);
            return null;
        }
    }
}
