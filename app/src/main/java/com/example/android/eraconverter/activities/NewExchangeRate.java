package com.example.android.eraconverter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.android.eraconverter.R;

public class NewExchangeRate extends AppCompatActivity {

    public static final String EXTRA_REPLY_BUNDLE =
            "com.example.android.eraconverter.BUNDLE";

    private EditText mCurr1Text;
    private EditText mCurr2Text;
    private EditText mRateText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_exchange_rate);

        mCurr1Text = findViewById(R.id.curr1_input);
        mCurr2Text = findViewById(R.id.curr2_input);
        mRateText = findViewById(R.id.rate_input);

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mCurr1Text.getText()) ||
                        TextUtils.isEmpty(mCurr2Text.getText()) ||
                        TextUtils.isEmpty(mRateText.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String curr1 = mCurr1Text.getText().toString();
                    String curr2 = mCurr2Text.getText().toString();
                    double rate = Double.parseDouble(mRateText.getText().toString());

                    Bundle bundle = new Bundle();
                    bundle.putString("curr1", curr1);
                    bundle.putString("curr2", curr2);
                    bundle.putDouble("rate", rate);

                    replyIntent.putExtra(EXTRA_REPLY_BUNDLE, bundle);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });

    }
}
