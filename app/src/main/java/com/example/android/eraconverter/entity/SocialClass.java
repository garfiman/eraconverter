package com.example.android.eraconverter.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;

@Entity(tableName = "socialclass_table", primaryKeys = {"quartile", "locationAndAge"})
public class SocialClass {

    @NonNull
    private String quartile;

    @NonNull
    private String locationAndAge;

    private double avgIncome;

    @NonNull
    private String currencyType;

    @NonNull
    private String job;

    @NonNull
    private String propertyDesc;

    public SocialClass(@NonNull String quartile, @NonNull String locationAndAge, double avgIncome, @NonNull String currencyType, @NonNull String job, @NonNull String propertyDesc) {
        this.quartile = quartile;
        this.locationAndAge = locationAndAge;
        this.avgIncome = avgIncome;
        this.currencyType = currencyType;
        this.job = job;
        this.propertyDesc = propertyDesc;
    }

    @NonNull
    public String getQuartile() {
        return quartile;
    }

    @NonNull
    public String getLocationAndAge() {
        return locationAndAge;
    }

    public double getAvgIncome() {
        return avgIncome;
    }

    @NonNull
    public String getCurrencyType() {
        return currencyType;
    }

    @NonNull
    public String getJob() {
        return job;
    }

    @NonNull
    public String getPropertyDesc() {
        return propertyDesc;
    }


}
