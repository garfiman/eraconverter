package com.example.android.eraconverter.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.eraconverter.R;
import com.example.android.eraconverter.entity.ExchangeRate;

import java.util.List;

public class ExchangeRateListAdapter extends RecyclerView.Adapter<ExchangeRateListAdapter.ExchangeViewHolder> {

    private final LayoutInflater mInflater;
    private List<ExchangeRate> mExchangeRates;

    public ExchangeRateListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @NonNull
    @Override
    public ExchangeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.exchangeraterecyclerview_item, parent, false);
        return new ExchangeViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ExchangeViewHolder holder, int position) {
        if (mExchangeRates != null) {
            ExchangeRate current = mExchangeRates.get(position);
            holder.exchangeRateItemView.setText(current.toString());
        } else {
            // Covers the case of data not being ready yet.
            holder.exchangeRateItemView.setText(R.string.no_value);
        }
    }

    public void setExchangeRates(List<ExchangeRate> exchangeRates){
        mExchangeRates = exchangeRates;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mExchangeRates != null)
            return mExchangeRates.size();
        else return 0;
    }

    public ExchangeRate getExchangeRateAtPosition (int position) {
        return mExchangeRates.get(position);
    }

    class ExchangeViewHolder extends RecyclerView.ViewHolder {

        private final TextView exchangeRateItemView;

        ExchangeViewHolder(@NonNull View itemView) {
            super(itemView);
            exchangeRateItemView = itemView.findViewById(R.id.textView);
        }
    }
}
