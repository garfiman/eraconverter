package com.example.android.eraconverter.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.android.eraconverter.entity.SocialClass;

import java.util.List;

@Dao
public interface SocialClassDao {

    @Delete
    void deleteSocialClass(SocialClass exchangeRate);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(SocialClass socialClass);

    @Query("SELECT * from socialclass_table ORDER BY locationAndAge ASC")
    LiveData<List<SocialClass>> getAllSocialClasses();

    @Query("SELECT DISTINCT locationAndAge from socialclass_table")
    LiveData<List<String>> getAllEras();

    @Query("SELECT * from socialclass_table LIMIT 1")
    SocialClass[] getAnySocialClass();

    @Query("SELECT quartile FROM socialclass_table WHERE (locationAndAge = :era) ORDER BY ABS( avgIncome - :income ) LIMIT 1")
    String getCurrentQuartile(double income, String era);

    @Query("SELECT * FROM socialclass_table WHERE (locationAndAge = :era) AND (quartile = :quartile)")
    SocialClass getSCfromQuartile(String quartile, String era);
}
