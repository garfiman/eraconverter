package com.example.android.eraconverter.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.android.eraconverter.R;
import com.example.android.eraconverter.entity.SocialClass;
import com.example.android.eraconverter.viewmodel.SocialClassViewModel;

import java.util.ArrayList;
import java.util.List;

public class EraConverter extends AppCompatActivity {
    private String mFromEra;
    private String mToEra;
    private double mIncome;

    private Spinner eraFromSpinner;
    private Spinner eraToSpinner;

    private ArrayAdapter<String> spinnerArrayAdapter;
    private List<String> spinnerArray = new ArrayList<>();

    private EditText mInitialCurrencyBox;

    private SocialClassViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_era_converter);

        mInitialCurrencyBox = findViewById(R.id.from_income);

        mViewModel = ViewModelProviders.of(this).get(SocialClassViewModel.class);


        final Observer<List<String>> spinnerObserver = new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable final List<String> newSpinnerArray) {
                spinnerArrayAdapter.clear();
                spinnerArray = newSpinnerArray;
                for (String currentValue : newSpinnerArray) {
                    spinnerArrayAdapter.add(currentValue);
                }
            }
        };

        mViewModel.getAllEras().observe(this, spinnerObserver);


        spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, spinnerArray);

        eraFromSpinner = findViewById(R.id.spinner_era_from);
        eraToSpinner = findViewById(R.id.spinner_era_to);

        eraFromSpinner.setAdapter(spinnerArrayAdapter);
        eraToSpinner.setAdapter(spinnerArrayAdapter);

        eraFromSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //set fromStr on selected item
                mFromEra = eraFromSpinner.getSelectedItem().toString();
                //if the other is non null set that one too
                if (eraToSpinner.getSelectedItem() != null) {
                    mToEra = eraToSpinner.getSelectedItem().toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //
            }

        });

        eraToSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //set fromStr on selected item
                mToEra = eraToSpinner.getSelectedItem().toString();
                //if the other is non null set that one too
                if (eraFromSpinner.getSelectedItem() != null) {
                    mFromEra = eraFromSpinner.getSelectedItem().toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //
            }

        });

    }

    boolean isInitialized() {
        return mFromEra != null && mToEra != null;
    }

    public void openSocialClassDb(View view) {
        Intent intent = new Intent(this, SocialClassDbViewActivity.class);
        startActivity(intent);
    }

    public void eraConvert(View view) {

        String mCurr = mInitialCurrencyBox.getText().toString();
        if (TextUtils.isEmpty(mCurr)) {
            mInitialCurrencyBox.setError("Nem írtál be összeget");
            return;
        }

        if (!isInitialized()) {
            Toast toast = Toast.makeText(this.getApplicationContext(), "Töltsd ki az összes mezőt", Toast.LENGTH_SHORT);
            toast.show();
            return;
        }

        String currentQuartile = mViewModel.getCurrentQuartile(Double.valueOf(mCurr), mFromEra);
        SocialClass nowClass = mViewModel.getSCfromQuartile(currentQuartile, mFromEra);
        SocialClass thenClass = mViewModel.getSCfromQuartile(currentQuartile, mToEra);

        Bundle bundle = new Bundle();

        bundle.putString("nowEra", mFromEra);
        bundle.putString("thenEra", mToEra);

        bundle.putDouble("nowIncome", Double.valueOf(mCurr));
        bundle.putDouble("thenIncome", thenClass.getAvgIncome());

        bundle.putString("nowCurrency", nowClass.getCurrencyType());
        bundle.putString("thenCurrency", thenClass.getCurrencyType());

        bundle.putString("nowJob", nowClass.getJob());
        bundle.putString("thenJob", thenClass.getJob());

        bundle.putString("nowProperty", nowClass.getPropertyDesc());
        bundle.putString("thenProperty", thenClass.getPropertyDesc());

        Intent intent = new Intent(this, EraConvertResult.class);
        intent.putExtra("erasBundle", bundle);

        startActivity(intent);
    }
}
