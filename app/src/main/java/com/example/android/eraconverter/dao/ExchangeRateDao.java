package com.example.android.eraconverter.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.android.eraconverter.entity.ExchangeRate;

import java.util.List;

@Dao
public interface ExchangeRateDao {

    @Query("SELECT * from exchange_rate_table ORDER BY currencyOne ASC")
    LiveData<List<ExchangeRate>> getAllExchangeRates();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(ExchangeRate exchangeRate);

    @Delete
    void deleteExchangeRate(ExchangeRate exchangeRate);

    @Query("SELECT rate from exchange_rate_table where currencyOne = :currOne and currencyTwo = :currTwo")
    LiveData<Double> getExchangeRateBetweenTwoCurrencies(String currOne, String currTwo);

    @Query("SELECT rate from exchange_rate_table where currencyOne = :currOne and currencyTwo = :currTwo")
    Double getExchangeRateBetweenTwoCurrenciesSimple(String currOne, String currTwo);

    @Query("SELECT currencyOne from exchange_rate_table UNION SELECT currencyTwo from exchange_rate_table")
    LiveData<List<String>> getAllCurrencies();

    @Query("SELECT * from exchange_rate_table LIMIT 1")
    ExchangeRate[] getAnyExchangeRate();
}
