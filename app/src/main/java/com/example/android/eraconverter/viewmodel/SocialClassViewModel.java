package com.example.android.eraconverter.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.android.eraconverter.db.SocialClassRepository;
import com.example.android.eraconverter.entity.SocialClass;

import java.util.List;

public class SocialClassViewModel extends AndroidViewModel {

    private SocialClassRepository mRepository;
    private LiveData<List<SocialClass>> mAllSocialClasses;
    private LiveData<List<String>> mAllEras;


    public SocialClassViewModel(@NonNull Application application) {
        super(application);
        mRepository = new SocialClassRepository(application);
        mAllSocialClasses = mRepository.getAllSocialClasses();
        mAllEras = mRepository.getAllEras();
    }

    public LiveData<List<String>> getAllEras() {
        return mAllEras;
    }

    public LiveData<List<SocialClass>> getAllSocialClasses() {
        return mAllSocialClasses;
    }

    public void insert(SocialClass socialClass) {
        mRepository.insert(socialClass);
    }

    public void deleteExchangeRate(SocialClass socialClass) {
        mRepository.deleteSocialClass(socialClass);
    }

    public String getCurrentQuartile(double income, String era) {
        return mRepository.getCurrentQuartile(income, era);
    }

    public SocialClass getSCfromQuartile(String quartile, String era) {
        return mRepository.getSCfromQuartile(quartile, era);
    }

}
