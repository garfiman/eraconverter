package com.example.android.eraconverter.db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.android.eraconverter.dao.ExchangeRateDao;
import com.example.android.eraconverter.entity.ExchangeRate;

@Database(entities = {ExchangeRate.class}, version = 1, exportSchema = false)
public abstract class ExchangeRateDatabase extends RoomDatabase {

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback() {

                @Override
                public void onOpen(@NonNull SupportSQLiteDatabase db) {
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
        }
    };
    private static ExchangeRateDatabase INSTANCE;

    public abstract ExchangeRateDao ExchangeRateDao();

    public static ExchangeRateDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ExchangeRateDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            ExchangeRateDatabase.class, "exchange_rate_database")
                            .addMigrations(MIGRATION_1_2)
                            .allowMainThreadQueries()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final ExchangeRateDao mDao;


        PopulateDbAsync(ExchangeRateDatabase db) {
            mDao = db.ExchangeRateDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {

            if (mDao.getAnyExchangeRate().length < 1) {

                mDao.insert(new ExchangeRate("Magyar forint, 2020", "Római sestercius, i.e. 4. sz.", 1.2));
                mDao.insert(new ExchangeRate("Római sestercius, i.e. 4. sz.", "Magyar forint, 2020", 0.83));
            }
            return null;
        }
    }

}
